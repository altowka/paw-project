# README

# Versions
* Ruby version: 2.7.0
* rails version: 7.0.4

# To start application:

* "bundle install" - install packages
* rake db:migrate - run migrations
* "rails server" - start server

# System dependencies and configuration

* If you use Linux or macOS system all you need is installing Ruby and rails. If you use Windows you can install Ubuntu on Windows and then install Ruby and rails

# Database creation

* This project is using sqlite3 database because is more comfortable to use external database, please do some changes in database.yml file:

- change "development" to "development2"

- insert this code in the end of file: 

```development:
    adapter: mysql2
    encoding: utf8mb4
    # For details on connection pooling, see Rails configuration guide
    # http://guides.rubyonrails.org/configuring.html#database-pooling
    pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>
    username: <username>
    database: <database name>
    password: <your password>
    host: <host eg mysql.agh.edu.pl>
```

# ViewComponent
ViewComponent is a lib to make reusable somponents. To create new component run: 

* ```bin/rails generate component <component name> <props>```
