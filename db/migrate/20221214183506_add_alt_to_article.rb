class AddAltToArticle < ActiveRecord::Migration[7.0]
  def change
    add_column :articles, :alt, :string
  end
end
