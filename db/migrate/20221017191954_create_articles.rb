class CreateArticles < ActiveRecord::Migration[7.0]
  def change
    create_table :articles do |t|
      t.string :title
      t.string :text
      t.string :image
      t.boolean :text_on_left

      t.timestamps
    end
  end
end
