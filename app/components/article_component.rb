# frozen_string_literal: true

class ArticleComponent < ViewComponent::Base
  def initialize(alt:, text:, src:, title:, textOnLeft: false)
    @text = text
    @src = src
    @title = title
    @textOnLeft = textOnLeft
    @alt = alt
  end

end
