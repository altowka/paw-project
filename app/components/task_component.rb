class TaskComponent < ViewComponent::Base
    def initialize(title:, desc:, status:)
      @desc = desc
      @status = status
      @title = title
    end
end