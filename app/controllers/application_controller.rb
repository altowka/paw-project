class ApplicationController < ActionController::Base
    before_action :authorized
    helper_method :logged_in?
    helper_method :current_user
    helper_method :all_articles
    
    
    def all_articles  
        Article.all
    end
    
    def current_user   
        User.find_by(id: session[:user_id])
    end

    def logged_in?
        # byebug
        !current_user.nil?  
    end

    def authorized
        redirect_to '/' unless logged_in?
     end
end
