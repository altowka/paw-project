class SessionsController < ApplicationController

  skip_before_action :authorized, only: [:new, :create, :welcome]
  
  def new
  end

  def create
    user = User.find_by(username: params[:username])
    if user && user.authenticate(params[:password])
        session[:user_id] = user.id 
        redirect_to '/'
    else
      flash.now[:alert] = "Incorrect email or password."
      render :new, status: :bad_request
    end
  end

  def destroy
    session[:user_id] = ''
    redirect_to '/'
  end

  def welcome
    
  end

  def login
  end

  def page_requires_login
  end
  
end
