json.extract! article, :id, :title, :text, :image, :text_on_left, :created_at, :updated_at
json.url article_url(article, format: :json)
